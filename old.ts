declare var $: any;

type Validator = {
    [key: string]: [string, (string) => boolean][]
}

type FormObject = { name: string, value: string }

const defaultss = {
    "email": [
        ["Should not be empty.", e => !!e],
        ["Should end with .com", e => /\.com$/.test(e)]
    ]
} as Validator

function _validate(el: string, validator: Validator = defaults) {
    const form = $(`#${el}`).serializeArray() as FormObject[]
    const validations = form.map(({ name, value }) => new Promise(error => {
        if (!validator[name]) { error() }
        else {
            Promise.all(validator[name].map(([reason, test]) => new Promise(pass => {
                if (!test(value)) { pass({ name, reason }) }
                pass('nope')
            })))
            .then(passes => {
                    debugger
                    if(passes.length) { error(passes) }
                    else { error('nope') }
                })
        }
    }))
    Promise.all(validations)
        .then(errors => {
            console.log(errors)
        })
}