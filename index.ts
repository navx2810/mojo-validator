declare var $: any;

type Collection = { [field: string]: { [reason: string]: (string) => boolean } }
type SerializedForm = [{ name: string, value: any }]

namespace mojo.validation {

    export const tests = {
        LONGER_THAN_THREE_CHARACTERS: v => /\.{3,}/.test(v),
        REQUIRED: v => /\.+/.test(v)
    }


    const defaults: Collection = {
        "email": {
            "Needs to be longer than 3 characters": tests.LONGER_THAN_THREE_CHARACTERS,
            "Email is a required field": tests.REQUIRED
        },
        "password": {
            "Needs to be longer than 3 characters": tests.LONGER_THAN_THREE_CHARACTERS,
            "Password is a required field": tests.REQUIRED
        }
    }

    export function test(el: string, validators: Collection = {}) {
        Object.assign(validators, defaults)
        const form = $(el).serializeArray() as SerializedForm
        const fields = form.filter(field => !!validators[field.name])
        const validations = fields.map(({ name, value }) => {
            const messages = []
            for(const reason in validators[name]) {
                if (!validators[name][reason](value)) { messages.push(reason) }
            }
            if(messages.length) { return { name, messages} }
        })
        return validations
    }

    export function print(el: string, errors: [{ name: string, messages: string[] }]) {
        const form = $(el)
        if(errors && errors.length) {
            errors.forEach(({ name, messages }) => {
                form.find(`[data-validation-for="${name}"]`)
                .append(messages.map(message => $('<li></li>').addClass("error").text(message)))
            })
        }
    }
}
